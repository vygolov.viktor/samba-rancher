#!/bin/bash

if [[ ! -f "/usr/local/samba/etc/smb.conf" ]]; then
	/usr/local/samba/bin/samba-tool domain provision --server-role=dc --use-rfc2307 --dns-backend=BIND9_DLZ --realm=OFFICE.AISA.RU --domain=OFFICE --adminpass=Ta7u5eJa --option="interfaces=lo br0" --option="bind interfaces only=yes"
	cp /usr/local/samba/private/krb5.conf /etc/krb5.conf
	sed -i 's/# database "dlopen \/usr\/local\/samba\/lib\/bind9\/dlz_bind9_11.so";/ database "dlopen \/usr\/local\/samba\/lib\/bind9\/dlz_bind9_11.so";/' /usr/local/samba/bind-dns/named.conf
	sed -i '/\[global\]/a interfaces = 10.69.0.151' /usr/local/samba/etc/smb.conf
fi

/usr/local/samba/sbin/samba -F

while true; do
	echo samba
	sleep 10
done
